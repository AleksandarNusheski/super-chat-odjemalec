$(document).ready(function() {
    var baseUrl = "https://chat.data-lab.si/api";
    var user = {id: 88, name: "Aleksandar Nusheski"};
    var nextMessageId = 0;
    var currentRoom = "Skedenj";

    //Naloži seznam sob
    $.ajax({
        url: baseUrl + "/rooms",
        type: "GET",
        success: function (data) {
            for (i in data) {
                $("#rooms").append(" \
                    <li class='media'> \
                        <div class='media-body room' style='cursor: pointer;'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                                </a> \
                                <div class='media-body'> \
                                    <h5>" + data[i] + "</h5> \
                                </div> \
                            </div> \
                        </div> \
                    </li>");
            }
            $(".room").click(spremembaSobe);
        }
    });

    //Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
    pridobivanjePogovorov = function(){
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
            type: "GET",
            success: function (data) {
                for (i in data) {
                    $("#messages").append(" \
                        <li class='media'> \
                            <div class='media-body'> \
                                <div class='media'> \
                                    <a class='pull-left' href='#'> \
                                        <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + data[i].user.id + ".jpg' /> \
                                    </a> \
                                    <div class='media-body'> \
                                        <small class='text-muted'>" + data[i].user.name + " | " + data[i].time + "</small> \
                                        <br>" + data[i].text + "<hr> \
                                    </div> \
                                </div> \
                            </div> \
                        </li>");
                    nextMessageId = data[i].id + 1;
                }
                setTimeout(function(){
                    pridobivanjePogovorov()
                }, 5000);
            }
        })
    }
    
    //Klicanje funkcije za začetek nalaganja pogovorov
    $("messages").html("");
    pridobivanjePogovorov();

    //Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
    posodabljanjeUporabnikov = function(){
        $.ajax({
            url: baseUrl + "/users/" + currentRoom,
            type: "GET",
            success: function (data) {
                $("#users").html("");
                for (i in data) {
                    $("#users").append(" \
                        <li class='media'> \
                            <div class='media-body'> \
                                <div class='media'> \
                                    <a class='pull-left' href='#'> \
                                        <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + data[i].id + ".jpg' /> \
                                    </a> \
                                    <div class='media-body'> \
                                        <h5>" + data[i].name + "</h5> \
                                    </div> \
                                </div> \
                            </div> \
                        </li>");
                }
                setTimeout(function(){
                    posodabljanjeUporabnikov()
                }, 5000);
            }
        })
    }
    
    //Klicanje funkcije za začetek posodabljanja uporabnikov
    posodabljanjeUporabnikov();
    
    //Definicija funkcije za pošiljanje sporočila
    posiljanjeSporocila = function(){
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({user: user, text: $("#message").val()}),
            success: function (data) {
                $("#message").val("");
            },
            error: function(err) {
                alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!");
            }
        })
    }
    
    //Enter Key Pressed in On Click handler za pošiljanje sporočila
    $("#message").on('keydown', function(event) {
        if (event.which == 13 || event.keyCode == 13)
            posiljanjeSporocila();
    });
    $("#send").click(posiljanjeSporocila);
    
    //Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
    //V razmislek: pomisli o morebitnih težavah!
    spremembaSobe = function(event){
        $("#messages").html("");
        $("#users").html("");
        currentRoom = event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
        nextMessageId = 0;
    }
    //On Click handler za menjavo sobe
    //Namig: Seznam sob se lahko naloži kasneje, kot koda, ki se izvede tu.
});